	
<!DOCTYPE html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="scripts/css/global.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	</head>
	<body>

		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-md-3 pt-5 pb-5">
					<?php if(isset($_GET['result']) AND $_GET['result'] == 'true'){echo '<div class="alert alert-success alert-dismissible fade show" role="alert">Tarefa Adicionada com Sucesso!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';}?>
					<?php if(isset($_GET['remove']) AND $_GET['remove'] == 'true'){echo '<div class="alert alert-success alert-dismissible fade show" role="alert">Tarefa Removida com Sucesso!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';}?>
					<div class="card">
						<div class="card-header"><strong>TODO LIST</strong></div>
  						<div class="card-body">
  							<!-- repetir -->
  							<?php
  								include 'scripts/php/conexao.php';
  								$sql = "SELECT * FROM Task";
  								$result = mysqli_query($con, $sql);
  								if (mysqli_num_rows($result) > 0) {
    								while($row = mysqli_fetch_assoc($result)) {
    									echo '<div class="form-check border-bottom pb-1 pt-1">';
    										if($row["is_finished"] == 1){
    											echo '<input class="form-check-input check" type="checkbox" id="'.$row["id"].'" checked>';
    										}else{
    											echo '<input class="form-check-input check" type="checkbox" id="'.$row["id"].'">';
    										}
  											echo '<label class="form-check-label" for="'.$row["id"].'">'.$row["description"].' <a href="scripts/php/excluir.php?id='.$row["id"].'"><i class="fas text-danger fa-trash-alt fa-fw"></i></a></label>';
 						 				echo '</div>';
    								}
								} else {
    								echo "Nenhuma tarefa adicionada.";
								}
  							?>
						</div>
						<div class="card-footer">
							<button type="button" class="btn btn-success btn-block" id="botao_add" data-toggle="modal" data-target="#exampleModal">Nova Tarefa</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 			<div class="modal-dialog" role="document">
    			<div class="modal-content">
     				<div class="modal-header">
        				<h5 class="modal-title" id="exampleModalLabel">Adicionar Nova Tafera</h5>
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          					<span aria-hidden="true">&times;</span>
        				</button>
      				</div>
      				<form action="scripts/php/adicionar.php" method="post">
      					<div class="modal-body">
    						<label for="descricao">Descrição da Tarefa</label>
    						<textarea class="form-control" name="descricao" id="descricao" rows="4" maxlength="1000">></textarea>
      					</div>
      					<div class="modal-footer">
      						<button type="submit" class="btn btn-success">Adicionar</button>
        					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
     				 	</div>
     				 </form>
    			</div>
 			</div>
		</div>

		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<script src="scripts/js/adicionar.js"></script>
	</body>
</html>